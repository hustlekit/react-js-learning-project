import React, {Component} from "react";
import './App.css';
import Game from "./Game"
import Demo from "./Demo";
import Rando from "./Rando";
import Button from "./Button";

class App extends Component {
    render() {
        return (
            <div className={`App`}>
                {/*<Game />*/}
                {/*<Demo animal={"Bobcat"} food={"Pineapple"}/>*/}
                {/*<Rando maxNum={7}/>*/}
                <Button />
            </div>
        )
    }
}

export default App;
