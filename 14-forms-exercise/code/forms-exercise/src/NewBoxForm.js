import React, {Component} from "react";

class NewBoxForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            height: '',
            width: '',
            color: ''
        }
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        this.props.addBox(this.state);
        this.setState({
            height: '',
            width: '',
            color: ''
        })
    }

    handleChange = (evt) => {
        this.setState({
            [evt.target.name]: evt.target.value
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor={'height'}>Height:</label>
                    <input
                        id={'height'}
                        name={'height'}
                        value={this.state.height}
                        onChange={this.handleChange}
                    />
                    <label htmlFor={'width'}>Width:</label>
                    <input
                        id={'width'}
                        name={'width'}
                        value={this.state.width}
                        onChange={this.handleChange}
                    />
                    <label htmlFor={'color'}>Color:</label>
                    <input
                        id={'color'}
                        name={'color'}
                        value={this.state.color}
                        onChange={this.handleChange}
                    />
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}

export default NewBoxForm;